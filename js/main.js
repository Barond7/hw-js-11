const button = document.querySelectorAll('.btn');

document.addEventListener('keydown', function (event) {
    for (let btr of button) {

        if (event.code === 'Key' + btr.innerHTML || event.key === btr.innerHTML) {

            btr.classList.add('active');
        } else {
            btr.classList.remove('active');
        }
    }
});